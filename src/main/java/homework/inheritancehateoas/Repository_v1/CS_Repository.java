package homework.inheritancehateoas.Repository_v1;

import homework.inheritancehateoas.entity.Car;
import homework.inheritancehateoas.entity_v1.Classz;
import homework.inheritancehateoas.entity_v1.Student;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Repository
@Transactional
public class CS_Repository {

    @PersistenceContext
    EntityManager entityManager;

    public void save(Classz classz){

        entityManager.persist(classz);

    }

    public Classz getClassz(Integer id){
        return entityManager.find(Classz.class, id);
    }

    public List<Classz> findAll(){
        Query query = entityManager.createQuery("SELECT e From Classz e");
        List<Classz> list = (List<Classz>)query.getResultList();
        return list;
    }

    public String remove(Integer id){
        String status = "not found!";
        Classz classz = entityManager.find(Classz.class, id);
        if(classz != null){
            entityManager.remove(classz);
            status = "removed!";
        }
        return  status;
    }


    public String updates(Classz classz){
        String status = "not found!";
        Student student = new Student();
        Classz classzz = entityManager.find(Classz.class, classz.getId());
        if(classzz != null){
            entityManager.merge(classz);
            status = "updateed!";
        }
        return status;
    }



}
