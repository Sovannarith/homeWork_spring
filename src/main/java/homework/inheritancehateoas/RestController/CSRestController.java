package homework.inheritancehateoas.RestController;

import homework.inheritancehateoas.Repository_v1.CS_Repository;
import homework.inheritancehateoas.entity.Car;
import homework.inheritancehateoas.entity_v1.Classz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("api")
public class CSRestController {

    @Autowired
    CS_Repository cs_repository;


    @PostMapping(value = "/save", produces = MediaType.APPLICATION_JSON_VALUE)
    public void save(@RequestBody Classz car){
        cs_repository.save(car);
    }

    @GetMapping(value = "/find/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Resource<Classz> find(@PathVariable Integer id){
        Classz car = new Classz();
        car = cs_repository.getClassz(id);
        if (car != null)
            return getClasszResource(car);
        else return null;
    }
    @GetMapping(value = "/findAll", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Resource<Classz>> findAll(){
        List<Classz> carList = cs_repository.findAll();
        List<Resource<Classz>> resources = new ArrayList<Resource<Classz>>();
        if(carList.size() == 0){
            resources.add(new Resource("Record Not Found!", ControllerLinkBuilder.linkTo(null).withRel("Save it First!")));
        }else {
            for (Classz car : carList) {
                resources.add(getClasszResource(car));
            }
        }
        return resources;
    }

    @DeleteMapping(value = "/delete/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Resource delete(@PathVariable Integer id){
        String status =  cs_repository.remove(id);
        return new Resource(status, ControllerLinkBuilder.linkTo(ControllerLinkBuilder.methodOn(CarRestController.class).findAll()).withRel("Car List"));
    }

    @PutMapping(value = "/updates")
    public String updates(@RequestBody Classz car){
        return cs_repository.updates(car);
    }


    public Resource<Classz> getClasszResource(Classz car){
        Resource<Classz> resource = new Resource<Classz>(car);

        resource.add(ControllerLinkBuilder.linkTo(ControllerLinkBuilder.methodOn(CarRestController.class).find(car.getId())).withSelfRel());

        resource.add(ControllerLinkBuilder.linkTo(ControllerLinkBuilder.methodOn(CarRestController.class).findAll()).withRel("Classz List"));

        resource.add(ControllerLinkBuilder.linkTo(ControllerLinkBuilder.methodOn(CarRestController.class).delete(car.getId())).withRel("Delete Classz"));

        return resource;
    }
}
