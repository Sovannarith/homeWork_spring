package homework.inheritancehateoas.RestController;

import homework.inheritancehateoas.Repository.CarRepository;
import homework.inheritancehateoas.entity.Bike;
import homework.inheritancehateoas.entity.Car;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/car")
public class CarRestController {

    @Autowired
    private CarRepository carRepository;

    @PostMapping(value = "/save", produces = MediaType.APPLICATION_JSON_VALUE)
    public void save(@RequestBody Car car){
        carRepository.save(car);
    }


    @GetMapping(value = "/find/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Resource<Car> find(@PathVariable Integer id){
        Car car = new Car();
        car = carRepository.find(id);
        if (car != null)
            return getCarResource(car);
        else return null;
    }

    @GetMapping("/findAll")
    public List<Resource<Car>> findAll(){
        List<Car> carList = carRepository.findAll();
        List<Resource<Car>> resources = new ArrayList<Resource<Car>>();
        if(carList.size() == 0){
            resources.add(new Resource("Record Not Found!",ControllerLinkBuilder.linkTo(null).withRel("Save it First!")));
        }else {
            for (Car car : carList) {
                resources.add(getCarResource(car));
            }
        }
        return resources;
    }

    @DeleteMapping(value = "/delete/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Resource delete(@PathVariable Integer id){
        String status =  carRepository.delete(id);
        return new Resource(status, ControllerLinkBuilder.linkTo(ControllerLinkBuilder.methodOn(CarRestController.class).findAll()).withRel("Car List"));
    }

    @PutMapping(value = "/updates")
    public String updates(@RequestBody Car car){
        return carRepository.updates(car);
    }


    public Resource<Car> getCarResource(Car car){
        Resource<Car> resource = new Resource<Car>(car);

        resource.add(ControllerLinkBuilder.linkTo(ControllerLinkBuilder.methodOn(CarRestController.class).find(car.getId())).withSelfRel());

        resource.add(ControllerLinkBuilder.linkTo(ControllerLinkBuilder.methodOn(CarRestController.class).findAll()).withRel("Car List"));

        resource.add(ControllerLinkBuilder.linkTo(ControllerLinkBuilder.methodOn(CarRestController.class).delete(car.getId())).withRel("Delete Car"));

        return resource;
    }
}
