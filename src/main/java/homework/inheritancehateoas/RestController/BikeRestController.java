package homework.inheritancehateoas.RestController;

import homework.inheritancehateoas.Repository.BikeRepository;
import homework.inheritancehateoas.entity.Bike;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/bike")
public class BikeRestController {

    @Autowired
    private BikeRepository bikeRepository;

    @PostMapping(value = "/save", produces = MediaType.APPLICATION_JSON_VALUE)
    public void save(@RequestBody Bike bike){
        bikeRepository.save(bike);
    }

    @GetMapping(value = "/find/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Resource<Bike> find(@PathVariable Integer id){
        Bike bike = new Bike();
        bike = bikeRepository.find(id);
        if (bike != null)
       return getBikeResource(bike);
        else return null;
    }

    @GetMapping("/findAll")
    public List<Resource<Bike>> findAll(){
        List<Bike> bikeList = bikeRepository.findAll();
        List<Resource<Bike>> resources = new ArrayList<Resource<Bike>>();
        if(bikeList.size() == 0){
            resources.add(new Resource("Record Not Found!",ControllerLinkBuilder.linkTo(null).withRel("Save it First!")));
        }else {
            for (Bike Bike : bikeList) {
                resources.add(getBikeResource(Bike));
            }
        }
        return resources;
    }

    @DeleteMapping(value = "/delete/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Resource delete(@PathVariable Integer id){
        String status =  bikeRepository.delete(id);
        return new Resource(status, ControllerLinkBuilder.linkTo(ControllerLinkBuilder.methodOn(BikeRestController.class).findAll()).withRel("Bike List"));
    }

    @PutMapping(value = "/updates")
    public String updates(@RequestBody Bike bike){
        return bikeRepository.updates(bike);
    }


    public Resource<Bike> getBikeResource(Bike Bike){
        Resource<Bike> resource = new Resource<Bike>(Bike);

        resource.add(ControllerLinkBuilder.linkTo(ControllerLinkBuilder.methodOn(BikeRestController.class).find(Bike.getId())).withSelfRel());

        resource.add(ControllerLinkBuilder.linkTo(ControllerLinkBuilder.methodOn(BikeRestController.class).findAll()).withRel("Bike List"));

        resource.add(ControllerLinkBuilder.linkTo(ControllerLinkBuilder.methodOn(BikeRestController.class).delete(Bike.getId())).withRel("Delete Bike"));

        return resource;
    }
}
