package homework.inheritancehateoas.entity_v1;

import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;

@Entity
public class Classz {

    @Id @GeneratedValue
    private Integer id;

    @Column(name = "className")
    private String name;

    @OneToOne (cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Student students;

    public Classz() {
    }

    public Classz(String name, Student students) {
        this.name = name;
        this.students = students;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    @Transactional
    public Student getStudents() {
        return students;
    }

    public void setStudents(Student students) {
        this.students = students;
    }
    @Override
    public String toString() {
        return "Classz{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", students=" + students +
                '}';
    }
}
