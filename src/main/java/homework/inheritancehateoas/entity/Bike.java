package homework.inheritancehateoas.entity;

import javax.persistence.Entity;

//@Entity
public class Bike extends Vehicle {

    private String bikeName;

    public Bike(){}

    public Bike(Integer id, String vehicleName) {
        super(id, vehicleName);
    }

    public Bike(Integer id, String vehicleName, String bikeName) {
        super(id, vehicleName);
        this.bikeName = bikeName;
    }

    public String getBikeName() {
        return bikeName;
    }

    public void setBikeName(String bikeName) {
        this.bikeName = bikeName;
    }

    @Override
    public String toString() {
        return "Bike{" +
                "bikeName='" + bikeName + '\'' +
                '}';
    }
}
