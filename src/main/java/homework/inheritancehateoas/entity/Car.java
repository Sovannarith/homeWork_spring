package homework.inheritancehateoas.entity;

import javax.persistence.Entity;

//@Entity
public class Car extends Vehicle {

    private String carName;

    public Car(){}

    public Car(Integer id, String vehicleName) {
        super(id, vehicleName);
    }

    public Car(Integer id, String vehicleName, String carName) {
        super(id, vehicleName);
        this.carName = carName;
    }

    public String getCarName() {
        return carName;
    }

    public void setCarName(String carName) {
        this.carName = carName;
    }

    @Override
    public String toString() {
        return "Car{" +
                "carName='" + carName + '\'' +
                '}';
    }
}
