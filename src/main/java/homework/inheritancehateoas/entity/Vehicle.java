package homework.inheritancehateoas.entity;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;

//@Entity
public abstract class Vehicle {

    @Id @GeneratedValue
    private Integer id;

    private String vehicleName;

    public Vehicle(){}
    public Vehicle(Integer id, String vehicleName) {
        this.id = id;
        this.vehicleName = vehicleName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getVehicleName() {
        return vehicleName;
    }

    public void setVehicleName(String vehicleName) {
        this.vehicleName = vehicleName;
    }

    @Override
    public String toString() {
        return "Vehicle{" +
                "id=" + id +
                ", vehicleName='" + vehicleName + '\'' +
                '}';
    }
}
