package homework.inheritancehateoas;

import ch.qos.logback.classic.joran.action.ConsolePluginAction;
import com.sun.deploy.uitoolkit.ui.ConsoleController;
import com.sun.deploy.uitoolkit.ui.ConsoleHelper;
import com.sun.deploy.uitoolkit.ui.ConsoleTraceListener;
import com.sun.deploy.uitoolkit.ui.ConsoleWindow;
import homework.inheritancehateoas.Repository.BikeRepository;
import homework.inheritancehateoas.Repository.CarRepository;
import homework.inheritancehateoas.Repository_v1.CS_Repository;
import homework.inheritancehateoas.entity.Bike;
import homework.inheritancehateoas.entity.Car;
import homework.inheritancehateoas.entity_v1.Classz;
import homework.inheritancehateoas.entity_v1.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.Transactional;

@SpringBootApplication
public class InheritanceHateoasApplication implements CommandLineRunner{

	public static void main(String[] args) {
		SpringApplication.run(InheritanceHateoasApplication.class, args);
	}

	@Override
	public void run(String... strings) throws Exception {


	}
}
