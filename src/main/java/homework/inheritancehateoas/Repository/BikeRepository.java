package homework.inheritancehateoas.Repository;

import homework.inheritancehateoas.entity.Bike;
import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Repository
@Transactional
public class BikeRepository {
    @PersistenceContext
    EntityManager em;

    public String save(Bike bike){
        String status = "Can not Save";
        try {
            em.persist(bike);
            status = "Saved!";
        }catch (HibernateException ed){
            status = "Can not Save";
        }
        return status;
    }

    public Bike find(Integer id){
        return em.find(Bike.class, id);
    }

    public List<Bike> findAll(){
        Query query = em.createQuery("SELECT e From Bike e");
        List<Bike> list = (List<Bike>)query.getResultList();
        return list;
    }
    public String delete(Integer id){
        String status = "not found!";
        Bike bike = em.find(Bike.class, id);
        if(bike != null){
            em.remove(bike);
            status = "deleted!";
        }
        return status;
    }
    public String updates(Bike bike){
        String status = "not found!";
        bike = em.find(Bike.class, bike.getId());
        if(bike != null){
            em.merge(bike);
            status = "updated!";
        }
        return status;
    }
}
