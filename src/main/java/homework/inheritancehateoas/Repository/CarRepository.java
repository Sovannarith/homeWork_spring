package homework.inheritancehateoas.Repository;

import homework.inheritancehateoas.entity.Bike;
import homework.inheritancehateoas.entity.Car;
import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Repository
@Transactional
public class CarRepository {

    @PersistenceContext
    EntityManager em;

    public String save(Car car){
        String status = "Can not Save";
        try {
            em.persist(car);
            status = "Saved!";
        }catch (HibernateException ed){
            status = "Can not Save";
        }
        return status;
    }

    public Car find(Integer id){
        return em.find(Car.class, id);
    }

    public List<Car> findAll(){
        Query query = em.createQuery("SELECT e From Car e");
        List<Car> list = (List<Car>)query.getResultList();
        return list;
    }
    public String delete(Integer id){
        String status = "not found!";
        Car car = em.find(Car.class, id);
        if(car != null){
            em.remove(car);
            status = "deleted!";
        }
        return status;
    }

    public String updates(Car car){
        String status = "not found!";
        car = em.find(Car.class, car.getId());
        if(car != null){
            em.merge(car);
            status = "updateed!";
        }
        return status;
    }

}
